# Neoteric Frontend Toolkit

Tweak Foundation grid, remove cruft

## Install

Add 'neoteric_fedkit' to your gemfile

$ rails g neoteric_fedkit:install

## Updating this gem

Important files:

lib/generators/neoteric_fedkit/install/templates/assets/stylesheets/
Main directory of SASS files. Based on ITCSS and BEM with a Foundation doing the heavy lifting of grid and basics.

lib/neoteric/fedkit/version.rb
Version file. Semantic versioning rules. Bump with with every public release.

test/dummy/app/views/layouts/application.html.erb
Demo page html.

### View features

$ bundle install
$ test/dummy/bin/rails server

### Publish a new version

- Install gemfury CLI if you don't have it
$ gem install gemfury

- Make your changes and commit them
- When ready to publish, bump version in version.rb appropriately with changes

- Package the gem
$ rake build

- git add the version change as a new commit
$ git add lib/neoteric_fedkit/version.rb
$ git commit -m "Bump NEW_VERSION_NUM"

- Tag last commit with version number
$ git tag NEW_VERSION_NUM

- Push changes out to Neoteric git server
$ git push

- Publish gem to gemfury
$ fury push pkg/neoteric_fedkit-NEW_VERSION_NUM.gem

