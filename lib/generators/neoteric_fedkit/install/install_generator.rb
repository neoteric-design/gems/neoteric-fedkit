module NeotericFedkit
  class InstallGenerator < ::Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    class_option :force_overwrite, :type => :boolean, :default => false

    def copy_views
      directory 'views', 'app/views', force: options[:force_overwrite]
    end

    def copy_assets
      directory 'assets', 'app/assets', force: options[:force_overwrite]
    end

    def copy_helpers
      directory 'helpers', 'app/helpers', force: options[:force_overwrite]
    end

    def stylesheet_extension
      %w(scss css.scss css).detect do |ext|
        File.exist? "app/assets/stylesheets/#{layout_name}.#{ext}"
      end
    end

    def layout_stylesheet
      return unless stylesheet_extension
      "app/assets/stylesheets/#{layout_name}.#{stylesheet_extension}"
    end

    def layout_name
      'application'
    end
  end
end
