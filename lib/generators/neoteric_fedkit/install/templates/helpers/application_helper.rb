# frozen_string_literal: true
module ApplicationHelper
  def primary_nav
    @primary_nav ||= ::PrimaryNavigation.new
  end

  def meta_description
    content_for(:meta_description) || ""
  end

  def og_image
    content_for(:og_image) || image_url('og-image.png')
  end
end
