//= require active_admin/base
//= require sociable/admin
//= require admin_toolbox/sortable_table
//= require articular
//= require evergreen
//= require flatpickr
//= require admin_stamp
//= require select-init

initDatePickers = function (scope) {
  $(scope).find('.date-picker').flatpickr({
    altInput: true
  })

  $(scope).find('.datetime-picker').flatpickr({
    altInput: true,
    enableTime: true,
    time_24hr: true
  })
}

$(function () {
  initDatePickers('body')
  initSelect2s('body')

  const districtSelect = document.getElementById('organization_district_id')

  if (districtSelect) {
    const subDistrictSelect = document.getElementById('organization_sub_district_id')

    $(document).on('change', '#organization_district_id', function () {
      const selectedDistrict = getSelectedLabel(districtSelect)

      subDistrictSelect.value = null
      filterOptionGroups(subDistrictSelect, selectedDistrict)
    })
    filterOptionGroups(subDistrictSelect, getSelectedLabel(districtSelect))
  }

  const sortables = ['districts', 'organization_types', 'specialties',
    'event_categories', 'news_categories', 'sub_districts', 'home_features']

  sortables.every(function (sortable) {
    $('table#index_table_' + sortable).sortable_table()
    return true
  })
})

function filterOptionGroups(element, groupLabel) {
  const labelSelector = '[label="' + groupLabel + '"]'
  const match = element.querySelector('optgroup' + labelSelector)
  const others = element.querySelectorAll('optgroup:not(' + labelSelector + ')')

  if (match) {
    match.removeAttribute('hidden')
    match.removeAttribute('disabled')
  }

  others.forEach((el) => {
    el.setAttribute('hidden', true)
    el.setAttribute('disabled', true)
  })
}

function getSelectedLabel(selectElement) {
  return selectElement.options[selectElement.selectedIndex].innerHTML
}
