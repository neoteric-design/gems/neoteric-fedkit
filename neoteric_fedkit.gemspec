$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "neoteric_fedkit/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name          = "neoteric_fedkit"
  s.version       = NeotericFedkit::VERSION
  s.authors       = ["Eric Wolinsky", "Matthew Cowie", "Peter Binkowski"]
  s.email         = ["eric@neotericdesign.com", "matthew@neotericdesign.com", "peter@neotericdesing.com"]

  if s.respond_to?(:metadata)
    s.metadata['allowed_push_host'] = "https://gemfury.com"
  end

  s.summary       = "Neoteric Design Front End Toolkit"
  s.homepage      = "http://www.neotericdesign.com"

  s.files = Dir["{app,config,db,lib,vendor}/**/*", "Rakefile", "README.md"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.2.0"
  s.add_dependency "foundation-rails", "> 6.0"
end
