require File.expand_path('../boot', __FILE__)

require "action_controller/railtie"
require "action_view/railtie"
require "sprockets/railtie"
require "rails/test_unit/railtie"
require 'foundation-rails'

Bundler.require(*Rails.groups)
require "neoteric_fedkit"

module Dummy
  class Application < Rails::Application
  end
end

