//= require rails-ujs
//= require jquery
//= require foundation

var ready = function () {
  $(document).foundation()
}

$(document).on('click', '.search-toggle', function (e) {
  e.stopPropagation()
  e.preventDefault()
  $(this).toggleClass('clicked')
  $('.search-bar').toggleClass('show-search')
  $('input#q').focus()
})

$(document).on('click', '#nav-icon', function () {
  console.log('clicked!')
  this.classList.toggle('active')
  $('body').toggleClass('fixed');
})

$(document).on('change', 'select.url-select', function (e) {
  const location = $(this).val()
  Turbolinks.visit(location)
})

$(document).on('down.zf.accordion', function (event, target) {
  const remote = $(target).data('remote')
  if (typeof (remote) === 'undefined') { return }

  $.getScript(remote)
  $(target).removeData('remote', null).removeAttr('data-remote', null)
})

$(window).scroll(function () {
  clearTimeout($.data(this, 'scrollTimer'))
  $('.dropdown').addClass('hidden')
  $.data(this, 'scrollTimer', setTimeout(function () {
    $('.dropdown').removeClass('hidden')
    console.log("Haven't scrolled in 250ms!")
  }, 100))
})

$(document).on('turbolinks:load', ready)
